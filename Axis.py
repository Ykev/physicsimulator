from Physics import *
X = 0
Y = 1
class Axis:
    def __init__(self, x0: int = 0, t: int = 0, v0: int = 0, a: int = 0):
        self.x0 = x0
        self.t = t 
        self.v0 = v0
        self.a = a
    def get_X_acceleration(self):
         return X_acceleration(self.x0, self.v0, self.t, self.a)

    def get_V_acceleration(self):
        return V_acceleration(self.v0, self.a, self.t)