from enum import Enum
from Axis import *

class ObjectType(Enum):
    OBJECT = 0
    BALL = 1
    BIRD = 2
    ROCKET = 3

class Object:
    def __init__(self, x0 : int = 0, y0 : int = 0, X_t: int = 0, Y_t: int  = 0,
                 Y_v0: int = 0, X_v0: int = 0, X_a: int = 0, Y_a: int = 0):

        self.X = Axis(x0=x0, t=X_t, v0=X_v0, a=X_a)
        self.Y = Axis(x0=y0, t=Y_t, v0=Y_v0, a=Y_a)
        self.motionTrack = []
        self.type = ObjectType.OBJECT

    def addMotion(self, x:int, y:int):
        self.motionTrack.append([x, y])
    def getMotion(self, index:int):
        return self.motionTrack[index]
    def setMotion(self, index:int, x:int, y:int):
        self.motionTrack[index] = [x, y]
    def setCurrentPlace(self):
        self.X.x0, self.Y.x0 = self.getLastMotion()
        self.X.v0 = self.X.get_V_acceleration()
        self.Y.v0 = self.Y.get_V_acceleration()
        self.X.t = self.Y.t = 0
    def getLastMotion(self):
        return self.motionTrack[-1]

    def stopMotion(self):
        self.setCurrentPlace()
        self.X.a = self.X.v0 = self.Y.a = self.Y.v0 = 0
