from Object import *
from Ball import Ball
from Bird import Bird
from Rocket import Rocket
from Physics import *

class Environment:
    def __init__(self, objects: list, floor, roof, rightWall, leftWall, y_gravity, x_gravity):
        self.objects = objects
        self.floor = floor
        self.roof = roof
        self.rightWall = rightWall
        self.leftWall = leftWall
        self.x_gravity = x_gravity
        self.y_gravity = y_gravity

    def setObjectsAcceleration(self):
        for object in self.objects:
            object.X.a += self.x_gravity
            object.Y.a += self.y_gravity

    def simulate(self, turtles: list):
        for motion_i in range(len(self.objects[0].motionTrack)):
            for object, tr in zip(self.objects, turtles):
                tr.goto(object.getMotion(motion_i))

    def calc(self, duration, frequency):
        self.setObjectsAcceleration()
        frameAmount = int(duration / frequency)
        for i in range(len(self.objects) * frameAmount):
            for object in self.objects:
                object.addMotion(object.X.get_X_acceleration(), object.Y.get_X_acceleration())

                if not (i % 70) and i:
                    if(object.type == ObjectType.BIRD):
                        object.jump()
                if (i==5) and (object.type == ObjectType.ROCKET):
                    object.setCurrentPlace()
                    ax, ay = object.getAccelerationToTarget(self.y_gravity)
                    angle = object.angleToHit(ax, ay)
                    object.changeThrust(angle, self.y_gravity)

                axisType = -1
                switcher = {
                    ObjectType.BALL: lambda: Ball.changeDirection(object, axisType),
                    ObjectType.BIRD: lambda: Bird.die(object),
                    ObjectType.ROCKET: lambda: Rocket.stopMotion(object)
                }

                if (object.getMotion(i)[X] > self.rightWall or object.getMotion(i)[X] < self.leftWall):
                    axisType = X
                    switcher[object.type]()
                if (object.getMotion(i)[Y] > self.roof or object.getMotion(i)[Y] < self.floor):
                    axisType = Y
                    switcher[object.type]()
                object.Y.t += frequency
                object.X.t += frequency