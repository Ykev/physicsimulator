from Object import *
class Ball(Object):
    def __init__(self, x0 : int = 0, y0 : int = 0, X_t: int = 0, Y_t: int  = 0,
                 Y_v0: int = 0, X_v0: int = 0, X_a: int = 0, Y_a: int = 0):
        super().__init__(x0, y0, X_t, Y_t, Y_v0, X_v0, X_a, Y_a)
        self.type = ObjectType.BALL

    def changeDirection(self, axis):
        selectedAxis = self.X
        if(axis):
            selectedAxis = self.Y

        selectedAxis.v0 = -1 * selectedAxis.get_V_acceleration()
        selectedAxis.x0 = super().getLastMotion()[axis]
        selectedAxis.t = 0
        self.getLastMotion()[axis] = selectedAxis.get_X_acceleration()