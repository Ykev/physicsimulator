from Bird import *
class Rocket(Object):
    def __init__(self, target: Object, refreshRate, maxThrust, x0: int = 0, y0: int = 0, X_t: int = 0, Y_t: int = 0,
                 Y_v0: int = 0, X_v0: int = 0, X_a: int = 0, Y_a: int = 0):
        super().__init__(x0=x0, y0=y0, X_t=X_t, Y_t=Y_t, Y_v0=Y_v0, X_v0=X_v0, X_a=X_a, Y_a=Y_a)
        self.type = ObjectType.ROCKET
        self.target = target
        self.thrustAcceleration = 0
        self.futureTime = 0.1
        self.refreshRate = refreshRate
        self.maxThrust = maxThrust


    def targertFuturePos(self, time):
        axis_x = self.target.X
        axis_y = self.target.Y
        x = X_acceleration(axis_x.x0, axis_x.v0, axis_x.t + time, axis_x.a)
        y = X_acceleration(axis_y.x0, axis_y.v0, axis_y.t + time, axis_y.a)
        return [x, y]

    def getAngleTo(self, target: object):
        pos = self.getLastMotion()
        targedPos = self.targertFuturePos(self.futureTime)
        y_diff = pos[Y]-targedPos[Y]
        x_diff = pos[X] - targedPos[X]
        if not (x_diff):
            angle = -90 * math.copysign(1, y_diff)
        else:
            angle = math.degrees(math.atan(y_diff / x_diff))
        if (x_diff > 0):
            angle += 180

        return round(angle, 2)

    def getAccelerationToTarget(self, gravity):
        doWhile = True
        acceleration_x = 0
        acceleration_y = 0
        futureTime = self.futureTime
        pos = self.getLastMotion()

        while(math.sqrt(acceleration_x**2 + acceleration_y**2) > self.maxThrust) or (doWhile):
            doWhile = False
            targetPos = self.targertFuturePos(futureTime)
            x_diff = targetPos[X] - pos[X]
            y_diff = targetPos[Y] - pos[Y]
            futureTime += 0.1
            acceleration_x = A_distance_time(x_diff, self.X.v0, futureTime)
            acceleration_y = A_distance_time(y_diff, self.Y.v0, futureTime)
            acceleration_y -= gravity
        self.thrustAcceleration = math.sqrt(acceleration_x**2 + acceleration_y**2)

        print("target_pos: ", targetPos, " | pos: ", pos)
        #self.motionTrack[-1] = targetPos
        print("distance_X: ", x_diff," | Vx = ", self.X.v0, " | time = ", futureTime)
        print("distance_Y: ", y_diff," | Vy = ", self.Y.v0, " | time = ", futureTime)
        return acceleration_x, acceleration_y

    def angleToHit(self, ax, ay):
        if(ax == 0):
            angle = 90 * math.copysign(1, ay)
        else:
            angle = math.degrees(math.atan(ay / ax))
            if (ax < 0):
                angle += 180
        print(f"angle: {angle} | ax = {ax}, ay = {ay}")

        return angle

    def changeThrust(self, angle, gravity):
        self.X.a = round(self.thrustAcceleration * math.cos(math.radians(angle)), 2)
        self.Y.a = round(self.thrustAcceleration * math.sin(math.radians(angle)) + gravity, 2)

        print("angle: ", angle, "  ------ xa", self.X.a, " ya: ", self.Y.a, " | thrust = ", self.thrustAcceleration)
