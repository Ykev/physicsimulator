from Object import *
class Bird(Object):
    def __init__(self, jumpSpeed, x0: int = 0, y0: int = 0, X_t: int = 0, Y_t: int = 0,
                 Y_v0: int = 0, X_v0: int = 0, X_a: int = 0, Y_a: int = 0):
        super().__init__(x0=x0, y0=y0, X_t=X_t, Y_t=Y_t, Y_v0=Y_v0, X_v0=X_v0, X_a=X_a, Y_a=Y_a)
        self.type = ObjectType.BIRD
        self.jumpSpeed = jumpSpeed

    def jump(self):
        self.Y.v0 = self.jumpSpeed
        self.Y.x0 = self.getLastMotion()[Y]
        self.Y.t = 0
    def die(self):
        super().stopMotion()
