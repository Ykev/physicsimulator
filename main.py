import turtle
from Object import *
from Ball import Ball
from Bird import Bird
from Rocket import Rocket
from Object import ObjectType
from Environment import Environment
X0 = 0
Y0 = 100
DURATION = 60
FREQUENCY = .1
G = -10
FLOOR = -270
ROOF = 280
RIGHT_WALL = 320
LEFT_WALL = -330
JUMP_SPEED = 40

def main():

    #ball1 = Ball(x0=X0, y0=FLOOR + 10, Y_v0=-80, X_v0=30)
    #bird = Bird(x0=X0, y0=Y0, Y_v0=0, X_v0=3)
    #rocket = Rocket(target=bird, thrust=30, futurTime=5, x0=X0, y0=Y0-100, Y_v0=10, X_v0=0, Y_a=0)
    ball1 = Ball(x0=RIGHT_WALL - 10, y0=FLOOR+10, Y_v0=90, X_v0=-60)
    bird = Bird(x0=X0, y0=Y0, jumpSpeed=JUMP_SPEED, Y_v0=15, X_v0=0)
    rocket = Rocket(target=ball1, maxThrust=30, refreshRate=5, x0=X0, y0=FLOOR+10, Y_v0=20, X_v0=60)
    objects = [ball1, bird, rocket]
    turtles = []
    for object in objects:
        tr = turtle.Turtle()
        tr.speed(0)
        tr.penup()
        tr.goto(object.X.x0, object.Y.x0)
        tr.pendown()
        tr.shape("circle")
        if(object.type == ObjectType.BIRD):
            tr.shape("turtle")
        elif (object.type == ObjectType.ROCKET):
            tr.shape("triangle")
        turtles.append(tr)

    environment = Environment(objects, FLOOR, ROOF, RIGHT_WALL, LEFT_WALL, x_gravity=0, y_gravity=G)
    print("calcing...")
    environment.calc(DURATION, FREQUENCY)

    print("starts simulator...")
    environment.simulate(turtles)

    print("done")

if __name__ == '__main__':
    main()

