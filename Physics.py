import math
ROUND_POINT = 5
def X_acceleration(x0, v0, t, a):
	x = x0 + v0*t + 0.5*a*t*t
	return round(x, ROUND_POINT)
def V_acceleration(v0, a, t):
	v = v0 + a*t
	return round(v, ROUND_POINT)
def X_const_velocity(x0, v, t):
	x = x0 + v*t
	return round(x, ROUND_POINT)
def T_distance_acceleretion(d, a):
	t = math.sqrt(abs((2*d)/a))
	return round(t, ROUND_POINT)
def A_distance_time(d, v0, t):
	a = 2 * (d - v0*t) / t**2
	return round(a, ROUND_POINT)
